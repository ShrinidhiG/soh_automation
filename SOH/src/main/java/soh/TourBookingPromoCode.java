package soh;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TourBookingPromoCode extends util
{
	
//Add your xpaths here
	
	public static By soh_Visit_Us = By.xpath("(//a[@href=\"/visit-us.html\"])[2]");
	public static By soh_tour_selection = By.xpath("//div[contains(text(),'Tours & Experiences')]");
	public static By soh_select_tour = By.xpath("//div[contains(text(),'The Sydney Opera House Tour')]");
	public static By soh_book_now = By.xpath("//div[@data-btn-type=\"BOOKNOW\"]");
	public static By soh_add_promo = By.xpath("//button[@class=\"btn btn-secondary btn-add\"]/span");
	public static By soh_promo_code_holder = By.xpath("//input[@title=\"promo code\"]");
	public static By soh_submit = By.xpath("//button[@aria-disabled=\"false\" and contains(text(),'Submit')]");
	public static By soh_combo_selection = By.xpath("//div[@class='combo-selector-container soh-module']/div/div/div/div[2]/div[@class='combo-select-button']");
	public static By soh_date_select = By.xpath("//a[@data-id=\"20180630\"]");	
	public static By soh_time_select = By.xpath("//span[contains(text(),'10:30am')]");
	public static By soh_ticket_select = By.xpath("(//button[@aria-label='Add Tickets'])[1]");
	public static By soh_add_to_cart = By.xpath("//button[contains(text(),'Add to cart')]");
	public static By soh_toCheckout = By.xpath("//button[@id='toCheckout']");
	public static By soh_next_month = By.xpath("//div[@class='calendar']/div[@class='month-nav']/button[@class='btn btn-next']");
	
	
	
	@Test
	public static void tourBookingPromoCode() throws InterruptedException
	{
		WebDriverWait wait = new WebDriverWait(driver, 50);
		sohLogIn.logIn();
		driver.findElement(soh_Visit_Us).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_tour_selection)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_select_tour)).click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_book_now)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_add_promo)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_promo_code_holder)).sendKeys("HANNATOURS");
		WebElement submitButton = driver.findElement(soh_submit);
		submitButton.click();
		Thread.sleep(5000);
		String expectedMessage = "HANNATOURSPromotion applied";
		String actualMessage = driver.findElement(By.xpath("//p[contains(text(),'Promotion applied')]")).getText();
		System.out.println(actualMessage);
		Assert.assertEquals(actualMessage, expectedMessage, "Promocode is applied successfully");
		WebElement comboSelection = driver.findElement(soh_combo_selection);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].scrollIntoView(true);", comboSelection);
		//js.executeScript("arguments[0].click();", comboSelection);
		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_combo_selection)).click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_next_month)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_date_select)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_time_select)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_ticket_select)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_ticket_select)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_add_to_cart)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_toCheckout)).click();
		String expectedText = "your order is complete.";
		Thread.sleep(3000);
		driver.findElement(By.id("card-holder-name")).sendKeys("Shrinidhi Gudi");
		  driver.findElement(By.id("card-number")).sendKeys("4111 1111 1111 1111");
		  driver.findElement(By.id("expiryDate")).sendKeys("03/20");
		  driver.findElement(By.id("security-code")).sendKeys("765");
		  Thread.sleep(2000);
		  driver.findElement(By.cssSelector("*[for='payment-disclaimer']")).click();
			driver.findElement(By.xpath("//button[text()='Place order']")).click();
			Thread.sleep(10000);
			//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[contains(text(),'your order is complete.')]")));
			String actualText = driver.findElement(By.xpath("//h3[contains(text(),'your order is complete.')]")).getText();
			//Assert.assertEquals(actualText, expectedText, "The message displayed matches with the Expected text");
			if(actualText.equals(expectedText))
			{
				System.out.println("test Pass");
			}else
			{
				System.out.println("Modify the Code");
			}
		
		
		
		
		
		
	}
}

package soh;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ticketDiscountPromotion extends util
{
	
//Add your Xpaths here
	public static By soh_username = By.id("username");
	public static By soh_password = By.id("pwd");
	public static By soh_user_login = By.xpath("(//div[@class=\'soh-box-content\']//button)[1]");
	public static By soh_book_now = By.xpath("(//div[contains(text(),'Book Now')])[1]");
	public static By soh_add_promo_button = By.xpath("//button[@class=\"btn btn-secondary btn-add\"]/span");
	public static By soh_promo_code_holder = By.xpath("//input[@title=\"promo code\"]");
	public static By soh_submit = By.xpath("//button[@aria-disabled=\"false\" and contains(text(),'Submit')]");
	public static By soh_date_select = By.xpath("//a[@data-id=\"20181023\"]");
	public static By soh_time_select = By.xpath("//span[contains(text(),'7:30pm')]");
	public static By soh_select_by_price = By.xpath("//a[@href='#bypricerange']");
	public static By soh_any_seat_selection = By.xpath("//a[@href ='#zone0']");
	public static By soh_select_seat = By.xpath("(//button[@aria-label=\"Add Tickets\"])[1]");
	public static By soh_add_to_cart = By.xpath("//button[contains(text(),'Add to Cart')]");
	public static By soh_go_to_cart = By.xpath("//button[contains(text(),'Go to cart')]");
	public static By soh_insurance_selection = By.xpath("//*[@id=\"insuranceToCart\"]/fieldset/p[1]/label");
	public static By soh_toCheckout = By.xpath("//button[@id='toCheckout']");
	public static By soh_signIn_post_cart = By.xpath("//a[contains(text(),'Sign in to your account')]");
	public static By soh_ticket_receiption_confirmation = By.xpath("(//button[text()='Select to confirm'])[1]");
	public static By soh_masterpass_payment = By.xpath("//label[contains(text(),'MasterPass')]");
	public static By soh_masterpass_login = By.xpath("//a[@id='access-masterpass']");
	public static By soh_frame_name = By.xpath("(//iframe[@name=\"MasterPass_frame\"])[1]");
	public static By soh_masterpass_username = By.xpath("//input[@placeholder=\"Email or Mobile Number\"]/input[1]/input");
	
	

//Add your Data
	public static final String username = "shrinidhigudi@yahoo.com";
	public static final String password = "Adobe@123@";
	
	
	
	
	
	
	
	@Test
	public static void eventsTicketPromo() throws InterruptedException
	{
		WebDriverWait wait = new WebDriverWait(driver, 50);
		Thread.sleep(5000);
		driver.get("https://www4.uat.sydneyoperahouse.com/my-account/login.html");
		driver.navigate().refresh();

		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_username)).sendKeys(username);
		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_password)).sendKeys(password);

		driver.findElement(soh_user_login).click();
		Thread.sleep(5000);
		driver.get("https://www4.uat.sydneyoperahouse.com/events/whats-on/bell-shakespeare/2018/julius-caesar-tickets.html");
		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_book_now)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_add_promo_button)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_promo_code_holder)).sendKeys("AES2018");
		Thread.sleep(5000);
		WebElement submitButton = driver.findElement(soh_submit);
		
		
		if(submitButton.isEnabled())
		{
			submitButton.click();
			String expectedMessage = "AES2018Promotion applied";
			String actualMessage = driver.findElement(By.xpath("//p[contains(text(),'Promotion applied')]")).getText();
			Assert.assertEquals(actualMessage, expectedMessage, "Promocode is applied successfully");
			wait.until(ExpectedConditions.visibilityOfElementLocated(soh_date_select)).click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(soh_time_select)).click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(soh_select_by_price)).click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(soh_any_seat_selection)).click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(soh_select_seat)).click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(soh_select_seat)).click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(soh_add_to_cart)).click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(soh_go_to_cart)).click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(soh_insurance_selection)).click();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(soh_toCheckout)).click();
		/*	WebElement signIn = driver.findElement(soh_signIn_post_cart);
			if(signIn.isDisplayed())
			{
				signIn.click();
			}*/
			wait.until(ExpectedConditions.visibilityOfElementLocated(soh_ticket_receiption_confirmation)).click();
			/*wait.until(ExpectedConditions.visibilityOfElementLocated(soh_masterpass_payment)).click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(soh_masterpass_login)).click();
			Thread.sleep(3000);
			driver.switchTo().frame(driver.findElement(soh_frame_name));
			
			wait.until(ExpectedConditions.visibilityOfElementLocated(soh_masterpass_username)).sendKeys("shrinidhigudi@yahoo.com");
			*/
			String expectedText = "your order is complete.";
			driver.findElement(By.id("card-holder-name")).sendKeys("Shrinidhi Gudi");
			  driver.findElement(By.id("card-number")).sendKeys("4111 1111 1111 1111");
			  driver.findElement(By.id("expiryDate")).sendKeys("03/20");
			  driver.findElement(By.id("security-code")).sendKeys("765");
			  Thread.sleep(2000);
			  driver.findElement(By.cssSelector("*[for='payment-disclaimer']")).click();
				driver.findElement(By.xpath("//button[text()='Place order']")).click();
				Thread.sleep(10000);
				//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[contains(text(),'your order is complete.')]")));
				String actualText = driver.findElement(By.xpath("//h3[contains(text(),'your order is complete.')]")).getText();
				//Assert.assertEquals(actualText, expectedText, "The message displayed matches with the Expected text");
				if(actualText.equals(expectedText))
				{
					System.out.println("test Pass");
				}else
				{
					System.out.println("Modify the Code");
				}
			
			
			
			
			
			
		}	
		
		
		
				
		
	}
}

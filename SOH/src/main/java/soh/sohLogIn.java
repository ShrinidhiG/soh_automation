package soh;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;

public class sohLogIn extends util
{
//Add the xpaths here
	public static By soh_login_header = By.xpath("//div[@id='soh-page']/div/header/div/div[4]");
	public static By soh_username = By.id("username");
	public static By soh_password = By.id("pwd");
	public static By soh_user_login = By.xpath("(//div[@class=\'soh-box-content\']//button)[1]");
	public static By soh_gift_vouchers = By.xpath("//section[@class='footer-menu']//a[contains(text(),'Gift Vouchers')]");
	public static By soh_amount_donation = By.xpath("//div[@class='button-sec']/ul/li[3]/button");
	public static By soh_gift_receiver = By.id("giftReceiver");
	public static By soh_gift_sender  = By.cssSelector("*[for='giftSender']");
	public static By soh_personal_message = By.id("message");
	public static By soh_email_recipient_button = By.xpath("//label[@for='otherEmailDelivery']");
	public static By soh_add_to_cart = By.xpath("//button[contains(text(),'Add to cart')]");
	public static By soh_recipient_mail = By.xpath("//label[contains(text(),'Email address')]");
	public static By soh_voucher_cart = By.xpath("//header[@class='soh-box-header']/div/h2[contains(text(),'Gift Voucher')]");
	public static By soh_checkOut = By.id("toCheckout");
	public static By soh_insurance = By.xpath("//*[@id='insuranceToCart']/fieldset/p[1]/label");
	
	
	
	public static WebDriverWait wait = new WebDriverWait(driver, 50);
//Add test data here
	public static final String username = "shrinidhigudi@yahoo.com";
	public static final String password = "Adobe@123@";
	
	

	
	
	@Test(priority=1)
	public static void logIn() throws InterruptedException
	{
		Thread.sleep(5000);
		driver.get("https://www4.uat.sydneyoperahouse.com/my-account/login.html");
		//driver.findElement(soh_login_header).click();
	//	driver.navigate().refresh();

		WebDriverWait wait=new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_username)).sendKeys(username);
		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_password)).sendKeys(password);
		//wait.ignoring(ElementNotFoundException.class).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("*[for='pwd'] + *"))).sendKeys("Infinity@123!@(");

		 driver.findElement(soh_user_login).click();
		 Thread.sleep(5000);
	}
	
	@Test(priority=2)
	public static void giftVouchers() throws InterruptedException
	{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		String ExpectedText = "Gift Vouchers";
		WebElement giftVoucher = driver.findElement(soh_gift_vouchers);
		String actualText = giftVoucher.getText();
		System.out.println(actualText);
		giftVoucher.click();
		Thread.sleep(2000);
		WebElement amountDonation = driver.findElement(soh_amount_donation);
		js.executeScript("arguments[0].scrollIntoView();", amountDonation);
		amountDonation.click();
		driver.findElement(soh_gift_receiver).sendKeys("Shrinidhi Gudi");
		WebElement giftSender = driver.findElement(soh_gift_sender);
		driver.findElement(soh_personal_message).sendKeys("This is for Testing Purpose");
		driver.findElement(soh_email_recipient_button).click();
		Thread.sleep(3000);
		WebElement enterEmail = driver.findElement(soh_recipient_mail);
		Actions actions = new Actions(driver);
		actions.moveToElement(enterEmail);
		actions.click();
		actions.sendKeys("gudi@adobe.com");
		actions.build().perform();
		driver.findElement(soh_add_to_cart).click();
		Thread.sleep(5000);
	}
	@Test(priority=3)
	public static void goToCart() throws InterruptedException
	{
		WebElement voucherInCart = driver.findElement(soh_voucher_cart);
		Assert.assertEquals(true, voucherInCart.isDisplayed());
		driver.findElement(soh_checkOut).click();
		Thread.sleep(5000);
	}
	
	@Test(priority=4)
	public static void checkOut() throws InterruptedException
	{
		String expectedText = "your order is complete.";
		driver.findElement(By.id("card-holder-name")).sendKeys("Shrinidhi Gudi");
		  driver.findElement(By.id("card-number")).sendKeys("4111 1111 1111 1111");
		  driver.findElement(By.id("expiryDate")).sendKeys("03/20");
		  driver.findElement(By.id("security-code")).sendKeys("765");
		  Thread.sleep(2000);
		/*  Boolean address = driver.findElement(By.id("address-country-billing")).isDisplayed();
		 if(address) {
		 
		  WebElement Single = driver.findElement(By.id("address-country-billing"));  
			Select dd = new Select(Single);
			dd.selectByValue("92");
			Thread.sleep(2000);
			driver.findElement(By.id("locationNewAddress")).sendKeys("Adarshapalm street");
			Thread.sleep(2000);
			driver.findElement(By.id("locationNewAddress2")).sendKeys("Bellandur");
			Thread.sleep(2000);
			driver.findElement(By.id("cityNewAddress")).sendKeys("Bangalore");
			Thread.sleep(2000);
			driver.findElement(By.id("postCodeNewAddress")).sendKeys("560035");
			Thread.sleep(2000);
			driver.findElement(By.xpath("//button[text()='Confirm address']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("(//div[@class='col-xs-12'])[3]/div/div/p")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[text()='Place order']")).click();
		 }
		else {*/
			driver.findElement(By.cssSelector("*[for='payment-disclaimer']")).click();
			driver.findElement(By.xpath("//button[text()='Place order']")).click();
			Thread.sleep(20000);
			//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[contains(text(),'your order is complete.')]")));
			String actualText = driver.findElement(By.xpath("//h3[contains(text(),'your order is complete.')]")).getText();
			//Assert.assertEquals(actualText, expectedText, "The message displayed matches with the Expected text");
			if(actualText.equals(expectedText))
			{
				System.out.println("test Pass");
			}else
			{
				System.out.println("Modify the Code");
			}
	}
	


	
	
}

    
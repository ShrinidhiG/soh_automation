package soh;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class tourBooking extends util
{

//Add your Xpath here	
	
	public static By soh_Visit_Us = By.xpath("(//a[@href=\"/visit-us.html\"])[2]");
	public static By soh_tour_selection = By.xpath("//div[contains(text(),'Tours & Experiences')]");
	public static By soh_select_tour = By.xpath("//div[contains(text(),'The Sydney Opera House Tour')]");
	public static By soh_book_now = By.xpath("//div[@data-btn-type=\"BOOKNOW\"]");
	public static By soh_add_promo = By.xpath("//button[@class=\"btn btn-secondary btn-add\"]/span");
	public static By soh_promo_code_holder = By.xpath("//input[@title=\"promo code\"]");
	public static By soh_submit = By.xpath("//button[@aria-disabled=\"false\" and contains(text(),'Submit')]");
	public static By soh_date_select = By.xpath("//a[@data-id=\"20180531\"]");	
	public static By soh_time_select = By.xpath("//span[contains(text(),'10:30am')]");
	public static By soh_ticket_select = By.xpath("//tr[@data-id=\"8430\"]/td[3]/button[@aria-label='Add Tickets']");
	public static By soh_add_to_cart = By.xpath("//button[contains(text(),'Add to cart')]");
	public static By soh_toCheckout = By.xpath("//button[@id='toCheckout']");
	public static By soh_signIn = By.xpath("//a[contains(text(),'Sign in to your account')]");
	public static By soh_username = By.cssSelector("*[for=\"signInEmail\"]");
	public static By soh_password = By.cssSelector("*[for=\"signInPassword\"]");
	public static By soh_login = By.xpath("(//button[contains(text(),'SUBMIT')])[2]");

	
	
//Add your Test Data
	
	public static final String username = "shrinidhigudi@yahoo.com";
	public static final String password = "Adobe@123@";
	
	
	@Test
	public static void tourBooking() throws InterruptedException
	{
		WebDriverWait wait = new WebDriverWait(driver, 50);
		//sohLogIn.logIn();
		driver.findElement(soh_Visit_Us).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_tour_selection)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_select_tour)).click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_book_now)).click();
		Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_add_promo)).click();
		Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_promo_code_holder)).sendKeys("HANNATOURS");
		Thread.sleep(5000);
		WebElement submitButton = driver.findElement(soh_submit);
		if(submitButton.isEnabled())
		{
			submitButton.click();
			Thread.sleep(5000);
			String expectedMessage = "HANNATOURSPromotion applied";
			String actualMessage = driver.findElement(By.xpath("//p[contains(text(),'Promotion applied')]")).getText();
			System.out.println(actualMessage);
			Thread.sleep(5000);
			Assert.assertEquals(actualMessage, expectedMessage, "Promocode is applied successfully");
			driver.findElement(soh_date_select).click();
			Thread.sleep(5000);
			driver.findElement(soh_time_select).click();
			Thread.sleep(5000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(soh_ticket_select)).click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(soh_ticket_select)).click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(soh_add_to_cart)).click();
			//driver.findElement(soh_add_to_cart).click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(soh_toCheckout)).click();
			Thread.sleep(3000);
			/*driver.findElement(soh_signIn).click();
			//wait.until(ExpectedConditions.visibilityOfElementLocated(soh_signIn)).click();
			driver.findElement(soh_username).sendKeys(username);
			driver.findElement(soh_password).sendKeys(password);
			//wait.until(ExpectedConditions.visibilityOfElementLocated(soh_username)).sendKeys(username);
			//wait.until(ExpectedConditions.visibilityOfElementLocated(soh_password)).sendKeys(password);
			wait.until(ExpectedConditions.visibilityOfElementLocated(soh_login)).click();*/
			String expectedText = "your order is complete.";
			driver.findElement(By.id("card-holder-name")).sendKeys("Shrinidhi Gudi");
			  driver.findElement(By.id("card-number")).sendKeys("4111 1111 1111 1111");
			  driver.findElement(By.id("expiryDate")).sendKeys("03/20");
			  driver.findElement(By.id("security-code")).sendKeys("765");
			  Thread.sleep(2000);
			  driver.findElement(By.cssSelector("*[for='payment-disclaimer']")).click();
				driver.findElement(By.xpath("//button[text()='Place order']")).click();
				Thread.sleep(10000);
				//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[contains(text(),'your order is complete.')]")));
				String actualText = driver.findElement(By.xpath("//h3[contains(text(),'your order is complete.')]")).getText();
				//Assert.assertEquals(actualText, expectedText, "The message displayed matches with the Expected text");
				if(actualText.equals(expectedText))
				{
					System.out.println("test Pass");
				}else
				{
					System.out.println("Modify the Code");
				}
			
			
			
		}
	}
}	
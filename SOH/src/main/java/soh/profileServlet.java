package soh;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class profileServlet extends util 
{	
	
//Add your xpaths here
	public static By post_Login_Icon = By.xpath("//a[@href='https://www4.uat.sydneyoperahouse.com/my-account/your-profile.html']/span");
	public static By soh_accountDetails_Edit = By.xpath("(//span[contains(text(),'Edit')])[1]");
//	public static By soh_salutation = By.cssSelector("*[id='title'] *[value='1']");
	public static By soh_salutation = By.id("title");
	public static By soh_firstName_Update = By.cssSelector("*[id='firstName']");
	public static By soh_lastName_Update = By.cssSelector("*[id='lastName']");
	public static By soh_tel_update = By.cssSelector("*[id='tel']");
	public static By soh_save_change_button = By.xpath("(//button[contains(text(),'Save changes')])[1]");
	
	
	@Test
	public static void profilUpdate() throws InterruptedException
	{
		
		sohLogIn.logIn();
		driver.findElement(post_Login_Icon).click();
		WebElement accountEdit = driver.findElement(soh_accountDetails_Edit);
		
		if(accountEdit.isDisplayed())
		{
			
			
			accountEdit.click();
			
			WebDriverWait wait = new WebDriverWait(driver, 50);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			/*WebElement salutation = (new WebDriverWait(driver, 30))
					  .until(ExpectedConditions.visibilityOfElementLocated(By.id("title")));*/
			
			wait.until(ExpectedConditions.visibilityOfElementLocated(soh_salutation));
			WebElement salutation = driver.findElement(soh_salutation);
			js.executeScript("arguments[0].scrollIntoView();", salutation);
			Select selectSalutation = new Select(salutation);
			selectSalutation.selectByVisibleText("Mr");
			driver.manage().timeouts().pageLoadTimeout(5,TimeUnit.SECONDS);
			driver.findElement(soh_firstName_Update).clear();
			driver.findElement(soh_firstName_Update).sendKeys("Shrinidhi");
			driver.findElement(soh_lastName_Update).clear();
			driver.findElement(soh_lastName_Update).sendKeys("Gudi");
			driver.findElement(soh_tel_update).clear();
			driver.findElement(soh_tel_update).sendKeys("8095367929");
			driver.findElement(soh_save_change_button).click();
			
			
		}
	}

}

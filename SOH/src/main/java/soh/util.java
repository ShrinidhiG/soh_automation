package soh;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;


public class util 
{
	public static final String Expected_Title = "Sydney Opera House - Opera House Sydney - Sydney Opera House";
	public static final String BASE_URL = "https://www4.uat.sydneyoperahouse.com/";
	public static final String EXPECT_ERROR = "User or Password is not valid";
	public static WebDriver driver;
	public static WebDriverWait  wait;
	public static By post_Login_Icon = By.xpath("//a[@href='https://www4.uat.sydneyoperahouse.com/my-account/your-profile.html']/span");
	
	
	@BeforeTest
	public static void setUp()
	{
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\gudi\\Pictures\\Sydney Opera House\\chromedriver.exe");
		driver = new ChromeDriver();
//		System.setProperty("webdriver.gecko.driver", "C:\\Users\\gudi\\Pictures\\Sydney Opera House\\geckodriver.exe");
//		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		//driver.get(util.BASE_URL);
		driver.manage().timeouts().pageLoadTimeout(50,TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
	}
	
	
	
/*	@AfterTest
	public static void logOut()
	{
//		driver.findElement(By.xpath("//p/span[contains(text(),'SG')]")).click();
		//driver.findElement(post_Login_Icon).click();
		driver.get("https://www4.uat.sydneyoperahouse.com/my-account/your-profile.html");
		driver.findElement(By.xpath("//a[contains(text(),'Log Out')]")).click();
		WebElement Login_Btn = driver.findElement(By.xpath("//div[@id='soh-page']/div/header/div/div[4]"));
		if(Login_Btn.isDisplayed())
		{
			System.out.println("Logout Successful");
		}else
		{
			System.out.println("Logout Unsuccessful");
		}
		driver.quit();
	}*/
}

package soh;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class PromoCodeInURL extends util
{
	
//Add your xpaths here
	public static By soh_book_now = By.xpath("//div[@data-btn-type=\"BOOKNOW\"]");
	public static By soh_date_select = By.xpath("//a[@data-id=\'20181031\']");	
	public static By soh_time_select = By.xpath("//span[contains(text(),'6:30pm')]");
	public static By soh_soh_price_range_select = By.xpath("//a[@href='#bypricerange']");
	public static By soh_select_anywhere = By.xpath("//span[contains(text(),'Anywhere in the theatre')]");
	public static By soh_ticket_select = By.xpath("(//button[@aria-label='Add Tickets'])[1]");
	public static By soh_add_to_cart = By.xpath("//button[contains(text(),'Add to Cart')]");
	public static By soh_username = By.id("username");
	public static By soh_password = By.id("pwd");
	public static By soh_user_login = By.xpath("(//div[@class='soh-box-content']//button)[1]");
	public static By soh_go_to_cart = By.xpath("//button[contains(text(),'Go to cart')]");
	public static By soh_toCheckout = By.xpath("//button[@id='toCheckout']");
	public static By soh_cart_signin = By.xpath("//a[contains(text(),'Sign in to your account')]");
	public static By soh_cart_email = By.cssSelector("*[for='signInEmail']");
	public static By soh_cart_password = By.cssSelector("*[for='signInPassword']");
	public static By soh_login_button = By.xpath("//button[@class='btn btn-primary btn-sign-in-submit' and contains(text(),'SUBMIT')]");
	
//Add test data	
    public static String soh_promo_url = "https://www4.uat.sydneyoperahouse.com/events/whats-on/bell-shakespeare/2018/julius-caesar-tickets.html?esc=AES2018";
    public static final String username = "shrinidhigudi@yahoo.com";
	public static final String password = "Adobe@123@";
	
	
	@Test
	public static void promoCodeURL() throws InterruptedException
	{
		sohLogIn.logIn();
		WebDriverWait wait = new WebDriverWait(driver, 50);
		driver.get(soh_promo_url);
		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_book_now)).click();
		String expectedMessage = "AES2018Promotion applied";
		String actualMessage = driver.findElement(By.xpath("//p[contains(text(),'Promotion applied')]")).getText();
		System.out.println(actualMessage);
		Thread.sleep(5000);
		Assert.assertEquals(actualMessage, expectedMessage, "Promocode is applied successfully");
		driver.findElement(soh_date_select).click();
		driver.findElement(soh_time_select).click();
		Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_soh_price_range_select)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_select_anywhere)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_ticket_select)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_ticket_select)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_add_to_cart)).click();
		/*wait.until(ExpectedConditions.visibilityOfElementLocated(soh_username)).sendKeys(username);
		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_password)).sendKeys(password);
		//wait.ignoring(ElementNotFoundException.class).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("*[for='pwd'] + *"))).sendKeys("Infinity@123!@(");

		 driver.findElement(soh_user_login).click();*/
		 Thread.sleep(5000);
		driver.findElement(soh_go_to_cart).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_toCheckout)).click();
		Thread.sleep(3000);
	
		
		 driver.findElement(By.xpath("//*[@id=\"insuranceToCart\"]/fieldset/p[1]/label")).click();
	      Thread.sleep(2000);
	      driver.findElement(By.xpath("//button[@id='toCheckout']")).click();
	      Thread.sleep(3000);
	      //WebElement cartSignIn = wait.until(ExpectedConditions.visibilityOfElementLocated(soh_cart_signin));
	   /*   if(cartSignIn.isDisplayed())
	      {
	    	  cartSignIn.click();
	    	  wait.until(ExpectedConditions.visibilityOfElementLocated(soh_cart_email)).sendKeys(username);
	  		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_cart_password)).sendKeys(password);
	    	  JavascriptExecutor js = (JavascriptExecutor) driver;
	    	  js.executeScript("document.getElementById('signInEmail').value='shrinidhigudi@yahoo.com';");
	    	  js.executeScript("document.getElementById('signInPassword').value='Adobe@123@';");
	    	  
	  		wait.until(ExpectedConditions.visibilityOfElementLocated(soh_login_button)).click();
	      }*/
	      Thread.sleep(3000);
	      driver.findElement(By.xpath("(//button[text()='Select to confirm'])[1]")).click();  
	      Thread.sleep(3000);
	      driver.findElement(By.xpath("//label[text()='MasterPass']")).click();
	      driver.findElement(By.xpath("//label[text()='Credit card']")).click();
		  driver.findElement(By.id("card-holder-name")).sendKeys("Shrinidhi Gudi");
		  driver.findElement(By.id("card-number")).sendKeys("4111 1111 1111 1111");
		  driver.findElement(By.id("expiryDate")).sendKeys("03/20");
		  driver.findElement(By.id("security-code")).sendKeys("765");
		  Thread.sleep(2000);
		  Boolean address = driver.findElement(By.id("address-country-billing")).isDisplayed();
		 if(address) {
		 
		  WebElement Single = driver.findElement(By.id("address-country-billing"));  
			Select dd = new Select(Single);
			dd.selectByValue("92");
			Thread.sleep(2000);
			driver.findElement(By.id("locationNewAddress")).sendKeys("Adarshapalm street");
			Thread.sleep(2000);
			driver.findElement(By.id("locationNewAddress2")).sendKeys("Bellandur");
			Thread.sleep(2000);
			driver.findElement(By.id("cityNewAddress")).sendKeys("Bangalore");
			Thread.sleep(2000);
			driver.findElement(By.id("postCodeNewAddress")).sendKeys("560035");
			Thread.sleep(2000);
			driver.findElement(By.xpath("//button[text()='Confirm address']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("(//div[@class='col-xs-12'])[3]/div/div/p")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[text()='Place order']")).click();
		 }
		else {
			driver.findElement(By.xpath("(//div[@class='col-xs-12'])[3]/div/div/p")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("//button[text()='Place order']")).click();
			
		}
	      
	  }
		
	}


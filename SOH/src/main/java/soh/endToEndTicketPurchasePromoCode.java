package soh;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

public class endToEndTicketPurchasePromoCode extends util
{
	
//Add your xpath here
	public static By soh_add_promo_button = By.xpath("//button[@class=\"btn btn-secondary btn-add\"]/span");
	public static By soh_promo_code_holder = By.xpath("//input[@title=\"promo code\"]");
	public static By soh_submit = By.xpath("//button[@aria-disabled=\"false\" and contains(text(),'Submit')]");
	public static By soh_go_to_cart = By.xpath("//button[contains(text(),'Go to cart')]");
	
	@Test
	public static void endToEndTicket() throws InterruptedException
	{
		JavascriptExecutor js = (JavascriptExecutor)driver;
		sohLogIn.logIn();
		driver.findElement(By.xpath("(//a[@href='/events.html'])[2]")).click();//to click events
		Thread.sleep(2000);
		driver.findElement(By.xpath("(//a[contains(text(),'Book Tickets Now')])[2]")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//div[contains(text(),'Book Now')]")).click();
		Thread.sleep(3000);
		driver.findElement(soh_add_promo_button).click();
		//wait.until(ExpectedConditions.visibilityOfElementLocated(soh_add_promo_button)).click();
		//wait.until(ExpectedConditions.visibilityOfElementLocated(soh_promo_code_holder)).sendKeys("CICERO");
		Thread.sleep(2000);
		driver.findElement(soh_promo_code_holder).sendKeys("CICERO");
		Thread.sleep(2000);
		WebElement submitButton = driver.findElement(soh_submit);
		submitButton.click();
		Thread.sleep(5000);
		String expectedMessage = "CICEROPromotion applied";
		String actualMessage = driver.findElement(By.xpath("//p[contains(text(),'Promotion applied')]")).getText();
		System.out.println(actualMessage);
		Thread.sleep(5000);
		Assert.assertEquals(actualMessage, expectedMessage, "Promocode is applied successfully");
		driver.findElement(By.xpath("//*[@id=\"content\"]/article/div/div[3]/main/div/div[4]/div/section/div[1]/div/div[2]/div[2]/div[5]/div[7]/a")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//span[contains(text(),'8:00pm')]")).click();
		//		Thread.sleep(3000);
		//		driver.findElement(By.xpath("//*[@id=\"content\"]/article/div/div[3]/main/div/div[5]/div/section/ul/li[2]/a")).click();
		Thread.sleep(3000);
		WebElement selectByPrice = driver.findElement(By.xpath("//a[@href='#bypricerange']"));
		js.executeScript("arguments[0].scrollIntoView(true);", selectByPrice);
		js.executeScript("arguments[0].click();", selectByPrice);
		
		//Thread.sleep(3000);
		WebElement selectAnyWhere = driver.findElement(By.xpath("//a[@href ='#zone79293']"));
		js.executeScript("arguments[0].scrollIntoView(true)", selectAnyWhere);
		js.executeScript("arguments[0].click();", selectAnyWhere);
		//Thread.sleep(3000);
		WebElement selectTicket = driver.findElement(By.xpath("//*[@id=\"zone79293\"]/table/tbody/tr/td[3]/button[2]"));
		js.executeScript("arguments[0].scrollIntoView(true)", selectTicket);
		js.executeScript("arguments[0].click()", selectTicket);
		//		Thread.sleep(3000);
		//		driver.findElement(By.xpath("//*[@id=\"zone79293\"]/table/tbody/tr/td[3]/button[2]")).click();
		 // Thread.sleep(3000);
		
		 WebElement addToCart =  driver.findElement(By.xpath("//button[contains(text(),'Add to Cart')]"));
		 js.executeScript("arguments[0].scrollIntoView(true)", addToCart);
		 //js.executeScript("arguments[0].click();", addToCart);
		 addToCart.click();
		  Thread.sleep(8000);
		  //driver.findElement(By.xpath("(//a[@href='https://www4.uat.sydneyoperahouse.com/shop/cart.html'])[2]")).click();
		  //wait.until(ExpectedConditions.visibilityOfElementLocated(soh_go_to_cart)).click();
		  WebElement goToCart = driver.findElement(soh_go_to_cart);
		  
		  js.executeScript("arguments[0].click();", goToCart);
		  //driver.findElement(soh_go_to_cart).click();
		 /* driver.findElement(By.xpath("//*[@id=\"insuranceToCart\"]/fieldset/p[1]/label")).click();
	      Thread.sleep(2000);
	      driver.findElement(By.xpath("//button[@id='toCheckout']")).click();
	      Thread.sleep(3000);*/
		  c
		 
	      driver.findElement(By.xpath("(//button[text()='Select to confirm'])[1]")).click();  
	      Thread.sleep(3000);
	      driver.findElement(By.xpath("//label[text()='MasterPass']")).click();
	      driver.findElement(By.xpath("//label[text()='Credit card']")).click();
		  driver.findElement(By.id("card-holder-name")).sendKeys("Shrinidhi Gudi");
		  driver.findElement(By.id("card-number")).sendKeys("4111 1111 1111 1111");
		  driver.findElement(By.id("expiryDate")).sendKeys("03/20");
		  driver.findElement(By.id("security-code")).sendKeys("765");
		  Thread.sleep(2000);
		  Boolean address = driver.findElement(By.id("address-country-billing")).isDisplayed();
		 if(address) {
		 
		  WebElement Single = driver.findElement(By.id("address-country-billing"));  
			Select dd = new Select(Single);
			dd.selectByValue("92");
			Thread.sleep(2000);
			driver.findElement(By.id("locationNewAddress")).sendKeys("Adarshapalm street");
			Thread.sleep(2000);
			driver.findElement(By.id("locationNewAddress2")).sendKeys("Bellandur");
			Thread.sleep(2000);
			driver.findElement(By.id("cityNewAddress")).sendKeys("Bangalore");
			Thread.sleep(2000);
			driver.findElement(By.id("postCodeNewAddress")).sendKeys("560035");
			Thread.sleep(2000);
			driver.findElement(By.xpath("//button[text()='Confirm address']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("(//div[@class='col-xs-12'])[3]/div/div/p")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[text()='Place order']")).click();
		 }
		else {
			driver.findElement(By.xpath("(//div[@class='col-xs-12'])[3]/div/div/p")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("//button[text()='Place order']")).click();
			
		}
	      
	  }
	}

